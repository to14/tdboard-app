package cat.itb.tdboard.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.preference.PreferenceManager
import cat.itb.tdboard.model.API
import cat.itb.tdboard.model.Llista
import cat.itb.tdboard.model.Tasca
import cat.itb.tdboard.model.User
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TDBViewModel(application: Application) : AndroidViewModel(application) {

    private val api = API.create()

    var llista = MutableLiveData<Llista>()
    var tasca = MutableLiveData<Tasca>()

    var usuari = MutableLiveData<User>()

    init {
        refresh()
    }

    fun refresh() {

        val prefs = PreferenceManager.getDefaultSharedPreferences(getApplication())

        val call : Call<User> = if (prefs.contains("userid")) {
            val userid = prefs.getInt("userid", -1)
            api.getUser(userid)
        } else {
            api.addUser(User(0, mutableListOf()))
        }



        call.enqueue(object : Callback<User> {
            override fun onResponse(call: Call<User>, response: Response<User>) {
                with (prefs.edit()) {
                    putInt("userid", response.body()!!.id)
                    usuari.value = response.body()
                    println(usuari.value)
                    apply()
                }
            }
            override fun onFailure(call: Call<User>, t: Throwable) {
                TODO("Not yet implemented")
            }
        })

    }

    fun import(codi : Int) {
        val call = api.getList(codi)
        call.enqueue(object: Callback<Llista> {
            override fun onResponse(call: Call<Llista>, response: Response<Llista>) {
                usuari.value!!.listas.add(response.body()!!)
                updateUser()
            }

            override fun onFailure(call: Call<Llista>, t: Throwable) {
                TODO("Not yet implemented")
            }

        })
    }

    fun importUser(id: Int) {
        val call = api.getUser(id)
        val prefs = PreferenceManager.getDefaultSharedPreferences(getApplication())

        call.enqueue(object : Callback<User> {
            override fun onResponse(call: Call<User>, response: Response<User>) {
                with (prefs.edit()) {
                    putInt("userid", response.body()!!.id)
                    usuari.value = response.body()
                    println(usuari.value)
                    apply()
                }
            }
            override fun onFailure(call: Call<User>, t: Throwable) {
                TODO("Not yet implemented")
            }
        })
    }

    fun add(llista : Llista) {
        select(llista)
        val call = api.addList(llista)
        call.enqueue(object : Callback<Llista> {
            override fun onResponse(call: Call<Llista>, response: Response<Llista>) {
                usuari.value!!.listas.add(response.body()!!)
                updateUser()
            }
            override fun onFailure(call: Call<Llista>, t: Throwable) {
                TODO("Poner un error o algo aqui")
            }
        })
    }

    fun updateUser() {
        val call = api.updateUser(usuari.value!!)
        call.enqueue(object: Callback<User> {
            override fun onResponse(call: Call<User>, response: Response<User>) {
                usuari.value = response.body()
            }
            override fun onFailure(call: Call<User>, t: Throwable) {
                TODO("Not yet implemented")
            }
        })
    }

    fun add(tasca : Tasca) {
        val call = api.addTask(tasca)
        call.enqueue(object : Callback<Tasca> {
            override fun onResponse(call: Call<Tasca>, response: Response<Tasca>) {
                llista.value!!.tasks.add(response.body()!!)
                update(llista.value!!)
            }
            override fun onFailure(call: Call<Tasca>, t: Throwable) {
                TODO("Not yet implemented")
            }
        })
    }

    fun remove(llista : Llista) {
        usuari.value!!.listas.remove(llista)
        val call = api.updateUser(usuari.value!!)
        call.enqueue(object : Callback<User> {
            override fun onResponse(call: Call<User>, response: Response<User>) {}
            override fun onFailure(call: Call<User>, t: Throwable) {
                TODO("Not yet implemented")
            }
        })
    }

    fun remove(tasca : Tasca) {
        llista.value!!.tasks.remove(tasca)
        update(llista.value!!)
        api.removeTask(tasca.id).enqueue(object : Callback<Int> {
            override fun onResponse(call: Call<Int>, response: Response<Int>) {}
            override fun onFailure(call: Call<Int>, t: Throwable) {
                TODO("Not yet implemented")
            }
        })
    }

    fun edit(title : String, color : String) {
        llista.value!!.title = title
        llista.value!!.color = color
        update(llista.value!!)
    }

    fun update(llista : Llista) {
        val call = api.updateList(llista)
        call.enqueue(object : Callback<Llista> {
            override fun onResponse(call: Call<Llista>, response: Response<Llista>) {}
            override fun onFailure(call: Call<Llista>, t: Throwable) {
                TODO("Not yet implemented")

            }
        })
    }

    fun update(tasca : Tasca) {
        api.updateTask(tasca).enqueue(object : Callback<Tasca> {
            override fun onResponse(call: Call<Tasca>, response: Response<Tasca>) {}
            override fun onFailure(call: Call<Tasca>, t: Throwable) {
                TODO("Not yet implemented")
            }
        })
    }

    fun select(llista : Llista) {
        this.llista.value = llista
    }

    fun select(tasca : Tasca) {
        this.tasca.value = tasca
    }

}