package cat.itb.tdboard.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import cat.itb.tdboard.R
import cat.itb.tdboard.adapter.LlistaAdapter
import cat.itb.tdboard.databinding.FragmentEditLlistaBinding
import cat.itb.tdboard.viewmodel.TDBViewModel

class EditLlistaFragment : Fragment() {

    private lateinit var bind : FragmentEditLlistaBinding
    private val model : TDBViewModel by activityViewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        bind = FragmentEditLlistaBinding.inflate(layoutInflater)
        return bind.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bind.inputTitle.setText(model.llista.value!!.title)
        bind.inputColor.setText(model.llista.value!!.color)

        bind.delete.setOnClickListener {
            model.remove(model.llista.value!!)
            findNavController().navigate(R.id.action_editLlista_to_llistes)
        }

        bind.save.setOnClickListener {
            val title = bind.inputTitle.text.toString()
            val color = bind.inputColor.text.toString()
            model.edit(title, color)
            findNavController().navigate(R.id.action_editLlista_to_llistes)
        }
    }

}