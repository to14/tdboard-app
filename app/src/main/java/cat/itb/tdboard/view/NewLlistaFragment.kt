package cat.itb.tdboard.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import cat.itb.tdboard.R
import cat.itb.tdboard.databinding.FragmentNewLlistaBinding
import cat.itb.tdboard.model.Llista
import cat.itb.tdboard.viewmodel.TDBViewModel
import kotlin.random.Random


class NewLlistaFragment : Fragment() {

    private lateinit var bind : FragmentNewLlistaBinding
    private val model : TDBViewModel by activityViewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        bind = FragmentNewLlistaBinding.inflate(layoutInflater)
        return bind.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bind.Crear.setOnClickListener {
            val colors  = listOf("#ccffcc","#ffffcc", "#ffcccc", "#ccccff", "#ffccff",
                                "#ccffff", "#ccffcc", "#ffffcc", "#ffcccc", "#ccccff")
            val randomColor = Random.nextInt(colors.size)
            val randomElement = colors[randomColor]

            val codi = bind.codillista.text.toString()
            if (codi.isNotEmpty()) {
                model.import(codi.toInt())
            } else {
                val newLlista = Llista(
                    0,
                    bind.ETLlista.text.toString(), randomElement,
                    mutableListOf()
                )
                model.add(newLlista)
            }

            findNavController().navigate(R.id.action_newLlista_to_llistes)
        }

        bind.Cancelar.setOnClickListener {

            findNavController().navigate(R.id.action_newLlista_to_llistes)
        }

    }
}