package cat.itb.tdboard.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import cat.itb.tdboard.R
import cat.itb.tdboard.databinding.FragmentNewTascaBinding
import cat.itb.tdboard.model.Tasca
import cat.itb.tdboard.viewmodel.TDBViewModel

class NewTascaFragment : Fragment() {

    private lateinit var bind : FragmentNewTascaBinding
    private val model : TDBViewModel by activityViewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        bind = FragmentNewTascaBinding.inflate(layoutInflater)
        return bind.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bind.save.setOnClickListener {
            model.add(Tasca(1, bind.inputTitle.text.toString(), bind.inputDescription.text.toString(), true))
            findNavController().navigate(R.id.action_newTasca_to_tasques)
        }

        bind.cancel.setOnClickListener {
            findNavController().navigate(R.id.action_newTasca_to_tasques)
        }

    }

}