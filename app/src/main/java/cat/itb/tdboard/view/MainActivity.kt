package cat.itb.tdboard.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import cat.itb.tdboard.R
import cat.itb.tdboard.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.Theme_TDBoard)
        super.onCreate(savedInstanceState)

        val bind = ActivityMainBinding.inflate(layoutInflater)
        setContentView(bind.root)

    }
}