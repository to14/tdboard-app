package cat.itb.tdboard.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import cat.itb.tdboard.R
import cat.itb.tdboard.adapter.TascaAdapter
import cat.itb.tdboard.databinding.FragmentLlistesBinding
import cat.itb.tdboard.databinding.FragmentTasquesBinding
import cat.itb.tdboard.model.Tasca
import cat.itb.tdboard.viewmodel.TDBViewModel

class TasquesFragment : Fragment() {

    private lateinit var bind : FragmentTasquesBinding
    private val model : TDBViewModel by activityViewModels()
    private lateinit var tascaAdapter : TascaAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        bind = FragmentTasquesBinding.inflate(layoutInflater)
        return bind.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        tascaAdapter = TascaAdapter(model.llista.value!!, this)

        model.llista.observe(this, {
            bind.codi.text = "Llista: " + it.id
        })

        with (bind.tasques) {
            setHasFixedSize(false)
            adapter = tascaAdapter
        }

        bind.add.setOnClickListener {
            findNavController().navigate(R.id.action_tasques_to_newTasca)
        }

    }

    fun onClick(tasca : Tasca) {
        tascaAdapter.toggle(tasca)
        model.update(tasca)
    }

    fun onLongClick(tasca : Tasca) {
        model.select(tasca)
        findNavController().navigate(R.id.action_tasques_to_editTasca)
    }

}