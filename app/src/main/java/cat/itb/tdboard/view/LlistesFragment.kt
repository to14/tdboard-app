package cat.itb.tdboard.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import cat.itb.tdboard.R
import cat.itb.tdboard.adapter.LlistaAdapter
import cat.itb.tdboard.databinding.FragmentLlistesBinding
import cat.itb.tdboard.model.Llista
import cat.itb.tdboard.viewmodel.TDBViewModel

class LlistesFragment : Fragment() {

    private lateinit var bind : FragmentLlistesBinding
    private val model : TDBViewModel by activityViewModels()
    private lateinit var llistaAdapter : LlistaAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        bind = FragmentLlistesBinding.inflate(layoutInflater)
        return bind.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        llistaAdapter = LlistaAdapter(this)

        model.usuari.observe(this, {
            llistaAdapter.setLlistes(it.listas)
        })

        with (bind.llistes) {
            setHasFixedSize(false)
            adapter = llistaAdapter
        }

        bind.add.setOnClickListener{
            findNavController().navigate(R.id.action_llistes_to_newLlista)
        }

        bind.config.setOnClickListener {
            findNavController().navigate(R.id.action_llistes_to_editUser)
        }

        bind.refresh.setOnRefreshListener {
            model.refresh()
            bind.refresh.isRefreshing = false
        }

    }

    fun onClick(llista : Llista) {
        model.select(llista)
        findNavController().navigate(R.id.action_llistes_to_tasques)
    }

    fun onLongClick(llista : Llista) {
        model.select(llista)
        findNavController().navigate(R.id.action_llistes_to_editLlista)
    }




}