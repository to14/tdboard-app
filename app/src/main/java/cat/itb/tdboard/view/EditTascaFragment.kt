package cat.itb.tdboard.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import cat.itb.tdboard.R
import cat.itb.tdboard.databinding.FragmentEditTascaBinding
import cat.itb.tdboard.viewmodel.TDBViewModel

class EditTascaFragment : Fragment() {

    private lateinit var bind : FragmentEditTascaBinding
    private val model : TDBViewModel by activityViewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        bind = FragmentEditTascaBinding.inflate(layoutInflater)
        return bind.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bind.inputTitle.setText(model.tasca.value!!.name)
        bind.inputDescription.setText(model.tasca.value!!.description)

        bind.delete.setOnClickListener {
            model.remove(model.tasca.value!!)
            findNavController().navigate(R.id.action_editTasca_to_tasques)
        }

        bind.save.setOnClickListener {
            val title = bind.inputTitle.text.toString()
            val description = bind.inputDescription.text.toString()
            model.tasca.value!!.name = title
            model.tasca.value!!.description = description
            model.update(model.tasca.value!!)
            findNavController().navigate(R.id.action_editTasca_to_tasques)

        }

    }

}