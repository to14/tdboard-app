package cat.itb.tdboard.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import cat.itb.tdboard.R
import cat.itb.tdboard.databinding.FragmentEditUserBinding
import cat.itb.tdboard.viewmodel.TDBViewModel

class EditUserFragment : Fragment() {

    private lateinit var bind : FragmentEditUserBinding
    private val model : TDBViewModel by activityViewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        bind = FragmentEditUserBinding.inflate(layoutInflater)
        return bind.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bind.id.setText(model.usuari.value!!.id.toString())

        bind.cancel.setOnClickListener {
            findNavController().navigate(R.id.action_editUser_to_llistes)
        }

        bind.save.setOnClickListener {
            model.importUser(bind.id.text.toString().toInt())
            findNavController().navigate(R.id.action_editUser_to_llistes)
        }
    }

}