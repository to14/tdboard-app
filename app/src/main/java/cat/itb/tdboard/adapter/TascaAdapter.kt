package cat.itb.tdboard.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.media.Image
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import cat.itb.tdboard.R
import cat.itb.tdboard.databinding.ItemTascaBinding
import cat.itb.tdboard.model.Llista
import cat.itb.tdboard.model.Tasca
import cat.itb.tdboard.view.TasquesFragment

class TascaAdapter (private val llista : Llista, val listener: TasquesFragment): RecyclerView.Adapter<TascaAdapter.ViewHolder>(){

    private lateinit var context : Context
    private lateinit var image: Image

    fun add(tasca: Tasca){
        llista.tasks.add(tasca)
        notifyItemInserted(llista.tasks.size-1)
    }

    fun toggle(tasca : Tasca) {
        tasca.status = !tasca.status
        notifyItemChanged(llista.tasks.indexOf(tasca))
    }

    inner class ViewHolder(view : View) : RecyclerView.ViewHolder(view) {
        val bind = ItemTascaBinding.bind(view)
        fun setListener(tasca: Tasca){
            bind.root.setOnClickListener{
                listener.onClick(tasca)
            }
            bind.root.setOnLongClickListener {
                listener.onLongClick(tasca)
                true // wtf?? v2
            }
        }
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TascaAdapter.ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.item_tasca,parent, false)
        return ViewHolder(view)
    }

    @SuppressLint("ResourceType")
    override fun onBindViewHolder(holder: TascaAdapter.ViewHolder, position: Int) {
        val tasca = llista.tasks[position]
        with (holder) {
            setListener(tasca)
            bind.title.setText(tasca.name)
            bind.description.setText(tasca.description)
            bind.status.setImageResource(if (!tasca.status) R.raw.strikethrough2 else 0)
        }
    }

    override fun getItemCount(): Int {
        return llista.tasks.size
    }

}