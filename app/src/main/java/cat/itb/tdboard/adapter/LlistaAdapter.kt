package cat.itb.tdboard.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import cat.itb.tdboard.R
import cat.itb.tdboard.databinding.ItemLlistaBinding
import cat.itb.tdboard.model.Llista
import cat.itb.tdboard.view.LlistesFragment
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlin.random.Random

class LlistaAdapter (private val listener : LlistesFragment) : RecyclerView.Adapter<LlistaAdapter.ViewHolder>() {

    private lateinit var context : Context
    private var llistes : MutableList<Llista> = mutableListOf()

    @SuppressLint("NotifyDataSetChanged") // annoying
    fun setLlistes(llistes : MutableList<Llista>) {
        this.llistes = llistes
        notifyDataSetChanged()
    }

    fun add(llista : Llista) {
        llistes.add(llista)
        notifyItemInserted(llistes.size-1)
    }

    inner class ViewHolder(view : View) : RecyclerView.ViewHolder(view) {
        val bind = ItemLlistaBinding.bind(view)
        fun setListener(llista : Llista) {
            bind.root.setOnClickListener {
                listener.onClick(llista)
            }
            bind.root.setOnLongClickListener {
                listener.onLongClick(llista)
                true // wtf????
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.item_llista, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val llista = llistes[position]
        with (holder) {
            setListener(llista)
            bind.llista.setText(llista.title)
            bind.llista.setBackgroundColor(Color.parseColor(llista.color))
            bind.llista.rotation = listOf(-3F, 3F)[Random.nextInt(2)] // O bien 3 o -3
        }
    }

    override fun getItemCount(): Int {
        return llistes.size
    }

}