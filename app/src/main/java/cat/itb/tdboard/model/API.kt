package cat.itb.tdboard.model

import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

interface API {

    //@GET()
    //fun get(@Url url : String) : Call<Cat>

    @GET("todolists")
    fun getLists() : Call<MutableList<Llista>>

    @GET("todolists/user/{id}")
    fun getMyLists(@Path("id") id : Int) : Call<MutableList<Llista>>

    @GET("todolists/{id}")
    fun getList(@Path("id") id : Int) : Call<Llista>

    @POST("todolists")        fun addList(@Body llista : Llista) : Call<Llista>
    @DELETE("todolists/{id}") fun removeList(@Path("id") id : Int) : Call<Int>
    @PUT("todolists")         fun updateList(@Body llista : Llista) : Call<Llista>

    @POST("todoitems")        fun addTask(@Body tasca : Tasca) : Call<Tasca>
    @DELETE("todoitems/{id}") fun removeTask(@Path("id") id : Int) : Call<Int>
    @PUT("todoitems")         fun updateTask(@Body tasca : Tasca) : Call<Tasca>

    @GET("todousers/{id}")    fun getUser(@Path("id") id : Int) : Call<User>
    @POST("todousers")        fun addUser(@Body user : User) : Call<User>
    @PUT("todousers")         fun updateUser(@Body user : User) : Call<User>

    companion object {

        val BASE_URL = "https://tdboard.herokuapp.com/"
        fun create() : API {
            val interceptor = HttpLoggingInterceptor()
                .setLevel(HttpLoggingInterceptor.Level.BODY)
            val client = OkHttpClient
                .Builder()
                .addInterceptor(interceptor)
                .build()
            val gson = GsonBuilder()
                .setLenient()
                .create()
            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build()
            return retrofit.create(API::class.java)
        }

    }

}