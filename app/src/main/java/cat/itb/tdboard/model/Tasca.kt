package cat.itb.tdboard.model

data class Tasca(val id : Int, var name : String, var description : String, var status : Boolean)
